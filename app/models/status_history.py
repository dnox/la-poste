from app import db


class StatusHistory(db.Model):
    __tablename__ = "status_history"

    id = db.Column(db.Integer, primary_key=True)
    letter_id = db.Column(db.Integer)
    status = db.Column(db.String(191))
    timestamp = db.Column(db.TIMESTAMP)
