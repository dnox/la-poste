from typing import Optional

from flask import jsonify, request
from flask.views import MethodView

from app.api.service_api import ServiceApi


class UpdateAllStatusesView(MethodView):

    def __init__(self, service_api: ServiceApi):
        self.service_api = service_api

    def get(self):
        status = request.args.get("status")
        return self._process_request(status=status)

    def post(self):
        # Status is just for the test and debug purposes.
        status = request.form.get("status")
        return self._process_request(status=status)

    def _process_request(self, status: Optional[str]):
        try:
            self.service_api.update_all_statuses(status=status)
            response_code = 200
            result = {"success": True}
        except Exception:
            result = {"success": False}
            response_code = 500
        return jsonify(result), response_code
