import os
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

from app.config import config

app = Flask(__name__)

CORS(app, origins="*", supports_credentials=True)

config_name = os.getenv("FLASK_CONFIG") or "default"
app.config.from_object(config[config_name])

db = SQLAlchemy()

def setup_app():
    from .api.la_poste_api import LaPosteApi
    from .views.update_letter import FetchAndUpdateLetterStatusView
    from .views.update_all import UpdateAllStatusesView
    from .api.db_api import DBApi
    from .api.service_api import ServiceApi

    db.init_app(app)
    app.app_context().push()
    db.create_all()

    db_client = DBApi(database=db)
    la_poste_client = LaPosteApi(cfg=app.config)
    service_api = ServiceApi(db_client=db_client, la_poste_client=la_poste_client)

    app.add_url_rule(
        '/get_and_update_status',
        view_func=FetchAndUpdateLetterStatusView.as_view("get_and_update_status", service_api=service_api),
        methods=["POST", "GET"],
    )
    app.add_url_rule(
        '/update_all_statuses',
        view_func=UpdateAllStatusesView.as_view("update_all_statuses", service_api=service_api),
        methods=["POST"],
    )

from . import models
from .views import *


setup_app()
