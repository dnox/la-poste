import datetime
from contextlib import contextmanager
from typing import List, Any

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm.scoping import scoped_session

from app.models.letter import Letter
from app.models.status_history import StatusHistory


class DBApi:

    def __init__(self, database: SQLAlchemy):
        self.db = database

    def create_or_update_letter_status(self, tracking_number: str, status: str):
        with self.session_scope() as tx:
            # Make everything in the same transaction. If something breaks we just rollback
            # everything instead of puutting db in incosistent state
            letter = tx.query(Letter).filter_by(tracking_number=tracking_number).first()
            if letter:
                letter.status = status
            else:
                letter = Letter(tracking_number=tracking_number, status=status)
                tx.add(letter)
            tx.flush()
            self._update_status_history(tx=tx, letter=letter)

    @staticmethod
    def _update_status_history(tx: scoped_session, letter: Letter):
        new_status = StatusHistory(letter_id=letter.id, status=letter.status, timestamp=datetime.datetime.utcnow())
        tx.add(new_status)

    def get_tracking_numbers(self) -> List[str]:
        with self.session_scope() as tx:
            letters = tx.query(Letter).all()
            return [letter.tracking_number for letter in letters]

    @contextmanager
    def session_scope(self):
        """Provide a transactional scope around a series of operations."""
        session = self.db.session
        try:
            yield session
            session.commit()
        except Exception as e:
            session.rollback()
            raise e
        finally:
            session.close()


