from typing import Dict

import requests as requests


class RemoteServiceException(Exception):
    def __init__(self, error_message, error_code):
        super().__init__(error_message)
        self.message = error_message
        self.error_code = error_code


class LaPosteApiMock:

    def __init__(self, return_status: str):
        self.get_status_calls = []
        self.return_status = return_status

    def get_status(self, tracking_number: str) -> str:
        self.get_status_calls.append(tracking_number)
        return self.return_status
