
class Config:
    SQLALCHEMY_DATABASE_URI = "sqlite:///test.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    LAPOSTE_URI = "https://api.laposte.fr/"

class DevelopmentConfig(Config):
    ENV_TYPE = "development"
    # The key shouldn't be stored in github
    LAPOSTE_API_KEY = "kgNDAH+6ErIxCwNNu9s9s4tNWAKS9t96U1Kf21VIrLHPebcDPOV0o1R1U6+OAreH"

class ProductionConfig(Config):
    ENV_TYPE = "production"
    # The key shouldn't be stored in github
    LAPOSTE_API_KEY = "kgNDAH+6ErIxCwNNu9s9s4tNWAKS9t96U1Kf21VIrLHPebcDPOV0o1R1U6+OAreH"

class IntegrationTestConfig(Config):
    ENV_TYPE = "test"
    SQLALCHEMY_DATABASE_URI = "sqlite:///integrationtest.db"

config = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
    "default": DevelopmentConfig,
    "integration_test": IntegrationTestConfig,
}
