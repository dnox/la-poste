import unittest

import json
import sys

from flask import Flask
from flask_cors import CORS
from app.models.letter import Letter
from app.models.status_history import StatusHistory


class TestLaPosteApi(unittest.TestCase):
    def setUp(self):
        from app.views.update_letter import FetchAndUpdateLetterStatusView
        from app.views.update_all import UpdateAllStatusesView
        from app.api.db_api import DBApi
        from app.api.service_api import ServiceApi
        from app.tests.mocks.la_poste_api_mock import LaPosteApiMock
        from app import db, config


        self.app = Flask(__name__)

        CORS(self.app, origins="*", supports_credentials=True)
        self.db = db
        db.init_app(self.app)
        self.app.app_context().push()
        config_name = "integration_test"
        self.app.config.from_object(config[config_name])

        with self.app.app_context():
            self.db.create_all()
            self.db.session.commit()

        db_client = DBApi(database=self.db)
        self.la_poste_client = LaPosteApiMock(return_status="TEST")
        service_api = ServiceApi(db_client=db_client, la_poste_client=self.la_poste_client)  # type: ignore

        self.app.add_url_rule(
            '/get_and_update_status',
            view_func=FetchAndUpdateLetterStatusView.as_view("update_single", service_api=service_api),
            methods=["GET"],
        )
        self.app.add_url_rule(
            '/get_and_update_all_statuses',
            view_func=UpdateAllStatusesView.as_view("get_and_update_all", service_api=service_api),
            methods=["GET"],
        )

    def tearDown(self) -> None:
        with self.app.app_context():
            self.db.drop_all()
            self.db.session.commit()

    def test_get_and_update_status(self):
        tracking_number = '3C00638718704'
        response = self.app.test_client().get('/get_and_update_status', query_string={'tracking_number': '3C00638718704'})
        self.assertEqual(
            json.loads(response.get_data().decode(sys.getdefaultencoding())),
            {"status": "TEST"}
        )

        self.assertEqual(self.la_poste_client.get_status_calls, [tracking_number])

        letter = self.db.session.query(Letter).filter_by(tracking_number=tracking_number).first()
        self.assertEqual(
            letter.status,
            "TEST"
        )

        status_history = self.db.session.query(StatusHistory).all()
        self.assertEqual(
            len(status_history),
            1
        )
        self.assertEqual(
            status_history[0].letter_id,
            letter.id
        )

if __name__ == "__main__":
    unittest.main()
