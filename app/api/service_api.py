from multiprocessing import Process
from typing import Optional


from app.api.db_api import DBApi
from app.api.la_poste_api import LaPosteApi


class ServiceApi:

    def __init__(self, db_client: DBApi, la_poste_client: LaPosteApi):
        self.la_poste_client = la_poste_client
        self.db_client = db_client

    def update_letter_status(self, tracking_number: str, status: str) -> str:
        # As the api doesn't work for me, status here is for debug/test purposes
        if not status:
            status = self.la_poste_client.get_status(tracking_number=tracking_number)
        self.db_client.create_or_update_letter_status(tracking_number=tracking_number, status=status)
        return status

    def update_all_statuses(self, status: Optional[str] = None):
        # I wouldn't use multiprocessing here, rather Celery or similar
        async_process = Process(
            target=self._update_all_statuses,
            kwargs={"status": status}
        )
        async_process.start()

    def _update_all_statuses(self, status: Optional[str] = None):
        tracking_numbers = self.db_client.get_tracking_numbers()
        for tracking_number in tracking_numbers:
            async_process = Process(
                target=self.update_letter_status,
                kwargs={"tracking_number": tracking_number, "status": status}
            )
            async_process.start()

