from typing import Optional

from flask import request, jsonify
from flask.views import MethodView

from app.api.la_poste_api import RemoteServiceException
from app.api.service_api import ServiceApi


class FetchAndUpdateLetterStatusView(MethodView):

    def __init__(self, service_api: ServiceApi):
        self.service_api = service_api

    def get(self):
        tracking_number = request.args.get("tracking_number")
        status = request.args.get("status")
        return self._process_request(tracking_number=tracking_number, status=status)

    def post(self):
        tracking_number = request.form.get("tracking_number")
        # Status is just for the test and debug purposes.
        status = request.form.get("status")
        return self._process_request(tracking_number=tracking_number, status=status)

    def _process_request(self, tracking_number: str, status: Optional[str]):

        try:
            status = self.service_api.update_letter_status(tracking_number=tracking_number, status=status)
            response_code = 200
            result = {"status": status}
        except RemoteServiceException as e:
            result = e.message
            response_code = e.error_code
        return jsonify(result), response_code
