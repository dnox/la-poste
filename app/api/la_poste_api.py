from typing import Dict

import requests as requests


class RemoteServiceException(Exception):
    def __init__(self, error_message, error_code):
        super().__init__(error_message)
        self.message = error_message
        self.error_code = error_code


class LaPosteApi:

    def __init__(self, cfg: Dict):
        self.host = cfg.get("LAPOSTE_URI")
        self.app_private_key = cfg.get("LAPOSTE_API_KEY")
        self.headers = {"X-Okapi-Key": self.app_private_key,
                        "accept": "application/json"}

    def get_status(self, tracking_number: str) -> str:
        response = requests.get(
            (self.host + "suivi/v2/idships/{id}?lang={lang}").format(id=tracking_number, lang="en_GB"),
            headers=self.headers)
        if not response.ok:
            # We must not expose internal exceptions, but this is for the exercise purposes
            raise RemoteServiceException(error_message=response.json(), error_code=response.status_code)
        status = response.json().get("status")
        return status
